using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerRigidbodyController : MonoBehaviour
{
    [SerializeField] private Vector2 moveCoefficient;
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        InputController.Moving += HandleMoving;
    }

    private void OnDisable()
    {
        InputController.Moving -= HandleMoving;
    }

    private void HandleMoving(Vector2 moveVector)
    {
        var force = CalculateForce(moveVector);
        _rigidbody.AddForce(force);
    }

    private Vector3 CalculateForce(Vector2 moveVector)
    {
        var z = moveVector.x * moveCoefficient.x;
        var x = moveVector.y * moveCoefficient.y;
        return new Vector3(-x, 0, z);
    }
}