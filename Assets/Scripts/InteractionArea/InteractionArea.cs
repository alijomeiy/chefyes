using UnityEngine;

namespace InteractionArea
{
    public class InteractionArea : MonoBehaviour
    {
        private CanvasGroup _group;
        protected Hand CurrentHand;

        private void Awake()
        {
            _group = GetComponentInChildren<CanvasGroup>();
            Initialize();
        }

        private void OnEnable()
        {
            InputController.KeyUp += HandleKeyUp;
            InputController.KeyPressed += HandleKeyPressed;
            Enable();
        }

        private void OnDisable()
        {
            InputController.KeyUp -= HandleKeyUp;
            InputController.KeyPressed -= HandleKeyPressed;
            Disable();
        }

        private void OnTriggerEnter(Collider other)
        {
            _group.alpha = 1;
            CurrentHand = other.GetComponentInChildren<Hand>();
        }

        private void OnTriggerExit(Collider other)
        {
            _group.alpha = 0;
            CurrentHand = null;
        }

        protected virtual void Enable()
        {
        }

        protected virtual void Disable()
        {
        }

        private void HandleKeyPressed(KeyCode keyCode)
        {
            if (!IsInteractable(keyCode)) return;
            Interact(keyCode);
        }

        protected virtual void HandleKeyUp(KeyCode obj)
        {
        }

        protected virtual bool IsInteractable(KeyCode code)
        {
            return CurrentHand is not null;
        }

        protected virtual void Interact(KeyCode code)
        {
        }

        protected virtual void Initialize()
        {
        }
    }
}