using System;
using System.Collections.Generic;
using System.Collections;
using Interactable;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InteractionArea
{
    public class Window : OneKeyInteractionArea
    {
        [Serializable]
        private class OrderScore
        {
            public string id;
            public int score;
        }

        private class OrderDetails
        {
            public string ID;
            public bool IsReady;

            public OrderDetails(string id)
            {
                ID = id;
                IsReady = false;
            }
        }

        [SerializeField] private Transform plate;
        [SerializeField] private List<OrderScore> scores;
        [SerializeField] private List<string> ingredients;
        private bool _isInPlay;
        private float _generatedOrderSecond = -10;
        private List<OrderDetails> _currentOrder;

        protected override void Enable()
        {
            base.Enable();
            GameManager.StateChanged += HandleNewState;
        }

        protected override void Disable()
        {
            base.Disable();
            GameManager.StateChanged -= HandleNewState;
        }

        private void HandleNewState(string newState)
        {
            if (newState == "Play")
            {
                StartOrdering();
            }
            else
            {
                StopOrdering();
            }
        }

        protected override bool IsInteractable(KeyCode code)
        {
            if (!base.IsInteractable(code)) return false;
            var id = GetIngredientID();
            return IsNotReadyIDInOrder(id);
        }

        private bool IsNotReadyIDInOrder(string id)
        {
            foreach (var orderDetails in _currentOrder)
            {
                if (orderDetails.ID == id && !orderDetails.IsReady)
                    return true;
            }

            return false;
        }

        private string GetIngredientID()
        {
            var ingredient = CurrentHand.Content.GetComponent<Ingredient>();
            if (ingredient is null) return "";
            return ingredient.ID;
        }

        protected override void Interact(KeyCode code)
        {
            base.Interact(code);
            var id = GetIngredientID();
            FindAndMakeDetailsReady(id);
            ChangeHandContentParent();
        }

        private void ChangeHandContentParent()
        {
            CurrentHand.Place(plate.position, Quaternion.identity, plate);
        }

        private void FindAndMakeDetailsReady(string id)
        {
            foreach (var orderDetails in _currentOrder)
            {
                if (orderDetails.ID != id || orderDetails.IsReady) continue;
                orderDetails.IsReady = true;
                return;
            }
        }

        private void StartOrdering()
        {
            _isInPlay = true;
            StartCoroutine(PlaceNewOrder());
        }

        private IEnumerator PlaceNewOrder()
        {
            while (_isInPlay)
            {
                if (IsCurrentOrderCompleted())
                {
                    AddPreviousOrderScore();
                    GenerateNewOrder();
                }

                yield return new WaitForEndOfFrame();
            }
        }

        private void AddPreviousOrderScore()
        {
            if (Math.Abs(_generatedOrderSecond + 10) < .01f) return;
            var delta = Time.time - _generatedOrderSecond;
            var intDelta = (int) delta;
            var sum = -intDelta;
            foreach (var orderDetails in _currentOrder)
            {
                var score = scores.Find(s => s.id == orderDetails.ID);
                sum += score.score;
            }

            GameManager.Announce("add score", sum.ToString());
        }

        private bool IsCurrentOrderCompleted()
        {
            if (_currentOrder is null) return true;
            foreach (var orderDetails in _currentOrder)
            {
                if (!orderDetails.IsReady)
                {
                    return false;
                }
            }

            return true;
        }

        private void GenerateNewOrder()
        {
            var count = Random.Range(2, 4);
            _currentOrder = new List<OrderDetails>(count);
            var s = "Order: ";
            for (var i = 0; i < count; i++)
            {
                var randomIndex = Random.Range(0, ingredients.Count);
                var newItem = ingredients[randomIndex];
                _currentOrder.Add(new OrderDetails(newItem));
                s += $" - {newItem} \n";
            }

            GetComponentInChildren<TextMeshProUGUI>().text = s;
            _generatedOrderSecond = Time.time;
        }

        private void StopOrdering()
        {
            _isInPlay = false;
        }
    }
}