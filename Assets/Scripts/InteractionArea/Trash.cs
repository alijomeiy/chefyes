using UnityEngine;

namespace InteractionArea
{
    public class Trash : OneKeyInteractionArea
    {
        protected override void Interact(KeyCode code)
        {
            base.Interact(code);
            CurrentHand.Empty();
        }
    }
}