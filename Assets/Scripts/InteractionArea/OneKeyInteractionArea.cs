using UnityEngine;

namespace InteractionArea
{
    public class OneKeyInteractionArea : InteractionArea
    {
        [SerializeField] protected KeyCode keyCode;

        protected override bool IsInteractable(KeyCode code)
        {
            var baseCondition = base.IsInteractable(code);
            var isTrueKeyPressed = code == keyCode;
            return baseCondition && isTrueKeyPressed;
        }
    }
}