using System;
using System.Collections.Generic;
using UnityEngine;

namespace InteractionArea
{
    public class Refrigerator : InteractionArea
    {
        [Serializable]
        private class RawItem
        {
            public string id;
            public KeyCode keyCode;
            public GameObject item;
        }

        [SerializeField] private List<RawItem> items;

        protected override void Interact(KeyCode code)
        {
            base.Interact(code);
            var id = ExtractKeyCodeItemID(code);
            var item = Pool.Get(id);
            CurrentHand.TryAdd(item.transform);
        }

        private string ExtractKeyCodeItemID(KeyCode keyCode)
        {
            for (var i = 0; i < items.Count; i++)
            {
                if (items[i].keyCode == keyCode)
                    return items[i].id;
            }

            Debug.LogError($"{keyCode} NOT FOUND!");
            return "";
        }

        protected override bool IsInteractable(KeyCode code)
        {
            var baseCondition = base.IsInteractable(code);
            var trueKeyPressed = IsItemKeyPressed(code);
            return baseCondition && trueKeyPressed;
        }

        private bool IsItemKeyPressed(KeyCode keyCode)
        {
            foreach (var rawItem in items)
            {
                if (rawItem.keyCode == keyCode)
                    return true;
            }

            return false;
        }

        protected override void Initialize()
        {
            base.Initialize();
            CreateItemPool();
        }

        private void CreateItemPool()
        {
            foreach (var raw in items)
            {
                Pool.Add(raw.id, raw.item);
            }
        }
    }
}