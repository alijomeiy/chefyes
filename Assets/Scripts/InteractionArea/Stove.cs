using Interactable;
using UnityEngine;

namespace InteractionArea
{
    public class Stove : OneKeyInteractionArea
    {
        [SerializeField] private Transform slot;
        private ICookable _cookable;

        protected override void Interact(KeyCode code)
        {
            base.Interact(code);
            if (!CurrentHand.IsEmpty)
            {
                PutOnStove();
                return;
            }

            RemoveFromStove();
        }

        private void RemoveFromStove()
        {
            _cookable.RemoveStove();
            var cookableTransform = slot.GetChild(0);
            CurrentHand.TryAdd(cookableTransform);
        }

        private void PutOnStove()
        {
            _cookable = CurrentHand.Content.GetComponent<ICookable>();
            _cookable.PutOnStove();
            CurrentHand.Place(slot.position, Quaternion.identity, slot);
        }


        protected override bool IsInteractable(KeyCode code)
        {
            if (!base.IsInteractable(code)) return false;
            var isSlotEmpty = slot.childCount == 0;
            if (!CurrentHand.IsEmpty && isSlotEmpty)
            {
                return IsHandContentCookable();
            }

            var handEmptyAndSlotFull = CurrentHand.IsEmpty && !isSlotEmpty;
            return handEmptyAndSlotFull;
        }

        private bool IsHandContentCookable()
        {
            var cookable = CurrentHand.Content.GetComponent<ICookable>();
            return cookable is not null;
        }
    }
}