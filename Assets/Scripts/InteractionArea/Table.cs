using Interactable;
using UnityEngine;

namespace InteractionArea
{
    public class Table : OneKeyInteractionArea
    {
        private IChoppable _choppablity;
        private bool _isInteractionStarted;

        protected override bool IsInteractable(KeyCode code)
        {
            var baseCondition = base.IsInteractable(code);
            if (!baseCondition || CurrentHand.IsEmpty) return false;
            var choppable = CurrentHand.Content.GetComponent<IChoppable>();
            var isCurrentContentChoppable = choppable is not null;
            return isCurrentContentChoppable;
        }

        private void Update()
        {
            if (CurrentHand is null && _isInteractionStarted)
            {
                StopChopping();
            }
        }

        protected override void Interact(KeyCode code)
        {
            base.Interact(code);
            _isInteractionStarted = true;
            ExtractChoppablity();
            StartChopping();
        }

        private void ExtractChoppablity()
        {
            _choppablity = CurrentHand.Content.GetComponent<IChoppable>();
        }

        protected override void HandleKeyUp(KeyCode code)
        {
            base.HandleKeyUp(code);
            if (code != keyCode || !_isInteractionStarted) return;
            StopChopping();
        }

        private void StartChopping()
        {
            _choppablity.Chop();
        }

        private void StopChopping()
        {
            _choppablity.StopChopping();
            _isInteractionStarted = false;
        }
    }
}