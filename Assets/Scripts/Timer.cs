using System;
using System.Collections;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] private int seconds;

    [Header("Texts")] [SerializeField] private TextMeshProUGUI minuteText;
    [SerializeField] private TextMeshProUGUI secondText;

    private IEnumerator _timerCoroutine;
    private CanvasGroup _canvasGroup;

    public Action TimerFinished;

    private void Awake()
    {
        _timerCoroutine = GetTimer();
        _canvasGroup = GetComponentInChildren<CanvasGroup>();
    }

    public void StartTimer(int timerSeconds)
    {
        seconds = timerSeconds;
        StartTimer();
    }

    public void StartTimer()
    {
        _timerCoroutine = GetTimer();
        StartCoroutine(_timerCoroutine);
        _canvasGroup.alpha = 1;
    }

    public void PauseTimer()
    {
        StopCoroutine(_timerCoroutine);
        _canvasGroup.alpha = 0;
    }

    public void ContinueTimer()
    {
        StartCoroutine(_timerCoroutine);
    }

    private IEnumerator GetTimer()
    {
        for (var i = 0; i < seconds; i++)
        {
            UpdateMinutesText(i);
            UpdateSecondsText(i);
            yield return new WaitForSeconds(1);
        }

        TimerFinished?.Invoke();
        OnTimerFinished();
    }

    protected virtual void OnTimerFinished()
    {
        
    }

    private void UpdateSecondsText(int passedSeconds)
    {
        var s = passedSeconds % 60;
        secondText.text = s.ToString();
    }

    private void UpdateMinutesText(int passedSeconds)
    {
        var m = passedSeconds / 60;
        var stringM = m.ToString();
        if (stringM != minuteText.text)
        {
            minuteText.text = stringM;
        }
    }
}