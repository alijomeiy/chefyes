using UnityEngine;

public class Hand : MonoBehaviour
{
    public bool IsEmpty => transform.childCount == 0;
    public GameObject Content => transform.GetChild(0).gameObject;

    public bool TryAdd(Transform item)
    {
        if (transform.childCount != 0) return false;
        item.SetParent(transform);
        item.SetLocalPositionAndRotation(Vector3.zero, Quaternion.identity);
        return true;
    }

    public void Empty()
    {
        var far = new Vector3(-100, -100, -100);
        while (transform.childCount != 0)
        {
            var child = transform.GetChild(0);
            child.SetParent(null);
            child.SetPositionAndRotation(far, Quaternion.identity);
        }
    }

    public void Place(Vector3 position, Quaternion rotation, Transform parent)
    {
        var child = transform.GetChild(0);
        child.SetParent(parent);
        child.SetPositionAndRotation(position, rotation);
    }
}