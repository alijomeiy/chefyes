using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTimer : Timer
{
    private void OnEnable()
    {
        StartTimer();
    }

    protected override void OnTimerFinished()
    {
        base.OnTimerFinished();
        GameManager.ChangeState("End");
    }
}
