using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Score : MonoBehaviour
{
    public static int HighScore { get; private set; }
    public static bool IsThisHighScore { get; private set; }
    private int _lastHighScore;
    private TextMeshProUGUI _scoreText;
    private int _score;

    private void Awake()
    {
        _scoreText = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        GameManager.News += HandleNews;
    }

    private void OnDisable()
    {
        GameManager.News -= HandleNews;
    }

    private void HandleNews(string title, string description)
    {
        if (title != "add score") return;
        if (!int.TryParse(description, out var score)) return;
        _score += score;
        _scoreText.text = _score.ToString();
        ControlHighScore();
    }

    private void ControlHighScore()
    {
        _lastHighScore = PlayerPrefs.GetInt("high", -1);
        if (_score <= _lastHighScore) return;
        PlayerPrefs.SetInt("high", _score);
        IsThisHighScore = true;
        HighScore = _score;

    }
}