using System.Collections.Generic;
using UnityEngine;
using System;

public class InputController : MonoBehaviour
{
    [SerializeField] private List<KeyCode> watchOutKeys;
    public static Action<KeyCode> KeyPressed;
    public static Action<Vector2> Moving;
    public static Action<KeyCode> KeyUp;

    private void Update()
    {
        CheckKeyListUp();
        ControlMoveInput();
        CheckKeyListPressed();
    }

    private static void ControlMoveInput()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        var move = new Vector2(horizontal, vertical);
        if (move.sqrMagnitude != 0)
        {
            Moving?.Invoke(move);
        }
    }

    private void CheckKeyListPressed()
    {
        for (var i = 0; i < watchOutKeys.Count; i++)
        {
            if (Input.GetKeyDown(watchOutKeys[i]))
            {
                KeyPressed?.Invoke(watchOutKeys[i]);
            }
        }
    }

    private void CheckKeyListUp()
    {
        for (var i = 0; i < watchOutKeys.Count; i++)
        {
            if (Input.GetKeyUp(watchOutKeys[i]))
            {
                KeyUp?.Invoke(watchOutKeys[i]);
            }
        }
    }
}