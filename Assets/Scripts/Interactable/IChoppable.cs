using System;
using UnityEngine;

namespace Interactable
{
    public interface IChoppable
    {
        public void Chop();
        public void StopChopping();
        public void OnChoppingComplete(Action callback);
    }
}