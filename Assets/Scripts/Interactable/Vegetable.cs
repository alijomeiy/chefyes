using System;
using System.Collections;
using UnityEngine;

namespace Interactable
{
    public class Vegetable : Ingredient, IChoppable
    {
        [SerializeField] private float chopDuration;
        private IEnumerator _choppingCoroutine;
        private Action _choppingCompletedCallback;
        private bool _isChopped;

        private void Awake()
        {
            ID = "vegetable";
        }

        public void Chop()
        {
            if (_isChopped) return;
            _choppingCoroutine = Chopping();
            StartCoroutine(_choppingCoroutine);
        }

        public void StopChopping()
        {
            StopCoroutine(_choppingCoroutine);
            StopTimer();
        }

        public void OnChoppingComplete(Action callback)
        {
            _choppingCompletedCallback += callback;
        }

        private IEnumerator Chopping()
        {
            StartTimer((int) chopDuration);
            yield return new WaitForSeconds(chopDuration);
            ChangeToChopped();
            FireEvent();
        }

        private void FireEvent()
        {
            _choppingCompletedCallback?.Invoke();
        }

        private void ChangeToChopped()
        {
            ID = "chopped vegetable";
            _isChopped = true;
            var lScale = transform.localScale;
            transform.localScale = new Vector3(
                lScale.x * 1.5f, lScale.y / 2, lScale.z * 1.5f);
        }
    }
}