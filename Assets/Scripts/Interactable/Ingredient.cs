using UnityEngine;

namespace Interactable
{
    public class Ingredient : MonoBehaviour
    {
        [SerializeField] protected Timer timer;
        [SerializeField] private string id;

        public string ID
        {
            protected set => id = value;
            get => id;
        }

        protected void StartTimer(int seconds)
        {
            timer.StartTimer(seconds);
        }

        protected void StopTimer()
        {
            timer.PauseTimer();
        }
    }
}