namespace Interactable
{
    public interface ICookable
    {
        public void PutOnStove();

        public void RemoveStove();
    }
}