using System.Collections;
using UnityEngine;

namespace Interactable
{
    public class Meat : Ingredient, ICookable
    {
        [SerializeField] private Material cookedMaterial;
        [SerializeField] private Material burnedMaterial;
        [SerializeField] private float cookDuration;
        [SerializeField] private float beforeBurnDuration;
        public MeatState State { get; private set; }
        private IEnumerator _cookCoroutine;
        private Renderer _renderer;

        private void Awake()
        {
            ID = "raw meat";
            State = MeatState.Raw;
            _renderer = GetComponent<Renderer>();
        }

        public void PutOnStove()
        {
            if (State != MeatState.Raw) return;
            _cookCoroutine = BeOnStove();
            StartCoroutine(_cookCoroutine);
        }

        public void RemoveStove()
        {
            StopCoroutine(_cookCoroutine);
            StopTimer();
        }

        private IEnumerator BeOnStove()
        {
            yield return new WaitForSeconds(cookDuration);
            Cook();
            yield return new WaitForSeconds(beforeBurnDuration);
            Burn();
        }

        private void Burn()
        {
            StartTimer((int) beforeBurnDuration);
            _renderer.material = burnedMaterial;
            State = MeatState.Burned;
            ID = "burned meat";
        }

        private void Cook()
        {
            StartTimer((int) cookDuration);
            _renderer.material = cookedMaterial;
            State = MeatState.Cooked;
            ID = "cooked meat";
        }
    }

    public enum MeatState
    {
        Raw,
        Cooked,
        Burned
    }
}