using UnityEngine;
using System;

public class EndMenu : MonoBehaviour
{
    [SerializeField] private GameObject highScoreText;

    private void OnEnable()
    {
        highScoreText.SetActive(Score.IsThisHighScore);
    }
}