using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    private void Update()
    {
        ControlUI();
    }

    private void ControlUI()
    {
        var state = GameManager.State;
        for (var i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var isActive = child.name == state;
            child.gameObject.SetActive(isActive);
        }
    }
}