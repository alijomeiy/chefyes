using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static string State { get; private set; }

    public static Action<string> StateChanged;
    public static Action<string, string> News;

    private void Awake()
    {
        State = "Menu";
    }

    public static void ChangeState(string newState)
    {
        State = newState;
        StateChanged?.Invoke(newState);
    }

    public static void Announce(string title, string details)
    {
        News?.Invoke(title, details);
    }
}