using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    private static Dictionary<string, Queue<GameObject>> _pools = new();

    public static GameObject Get(string id)
    {
        if (!_pools.ContainsKey(id))
        {
            Debug.LogError($"<b>[CHEFYES]:</b> '{id}' NOT FOUND!");
            return null;
        }

        var obj = _pools[id].Dequeue();
        _pools[id].Enqueue(obj);
        return obj;
    }

    public static bool ContainID(string id)
    {
        return _pools.ContainsKey(id);
    }

    public static void Add(string id, GameObject instance, int count = 200)
    {
        if (ContainID(id))
        {
            Debug.LogError($"<b>[CHEFYES]:</b> '{id}' used previously!");
            return;
        }

        _pools.Add(id, new Queue<GameObject>(count));
        for (var i = 0; i < count; i++)
        {
            var o = Instantiate(instance);
            _pools[id].Enqueue(o);
        }
    }
}